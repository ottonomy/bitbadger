# BitBadger UI
This app allows a user to award an Open Badges badge easily from the comfort
of their browser. This uses signed badge verification, so the user imports a
key into local storage to sign badges with.

* Workflow
  - Import profile
  - register private key
  - view profile/badgelist
  - define badge
  - view badge/assertionlist
  - issue badge
  - view assertion
  - export assertion as JWS

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
