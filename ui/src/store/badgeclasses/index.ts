import { Module } from 'vuex';

import { RootState } from '../types';
import { BadgeClass } from './types';

import { getters } from './getters';
// import { actions } from './actions';
import { mutations } from './mutations';
import { BadgeClassState } from './types';

const sampleBadge: BadgeClass = {
  id: 'http://localhost:8000/badgeclass',
  name: 'Sample Badge',
  description: 'This is a sample BadgeClass, my dude.',
  criteria: {
    id: 'http://example.com/somebadge/criteria',
    narrative: 'Do some important things',
  },
  image: 'http://placehold.it/400/400',
  issuer: 'http://localhost:8000/profile',
};
export const state: BadgeClassState = {
  badges: [sampleBadge],
};

const namespaced: boolean = true;

export const badgeclasses: Module<BadgeClassState, RootState> = {
  namespaced,
  state,
  getters,
  // actions,
  mutations,
};
