interface Alignment {
  targetUrl: string;
  targetName: string;
  targetDescription?: string;
  targetFramework?: string;
  targetCode?: string;
}

export interface BadgeClass {
  id: string;
  name: string;
  description: string;
  image: string;
  criteria: Criteria;
  tags?: string[];
  issuer: string;
  alignment?: Alignment[];
}

export interface BadgeClassState {
  badges: BadgeClass[];
}

interface Criteria {
  narrative?: string;
  id?: string;
}
