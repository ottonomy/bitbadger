import { MutationTree } from 'vuex';
import { BadgeClass, BadgeClassState } from './types';

export const mutations: MutationTree<BadgeClassState> = {
    addBadgeClass(state: BadgeClassState, data: BadgeClass) {
        state.badges.push(data);
    },
};
