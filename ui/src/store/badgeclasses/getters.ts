import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { Profile } from '../common/types';
import { BadgeClassState, BadgeClass } from './types';


export const getters: GetterTree<BadgeClassState, RootState> = {
    getBadgesByTag(state: BadgeClassState, tag: string): BadgeClass[] {
        return state.badges.filter((badge: BadgeClass) => badge.tags && badge.tags.indexOf(tag) > -1);
    },
    getBadgesByProfile(state: BadgeClassState, _, rootState: RootState): BadgeClass[] {
        const currentProfileId = rootState.profiles.data[0].id;
        return state.badges.filter((badge: BadgeClass) => badge.issuer === currentProfileId);
    },
};
