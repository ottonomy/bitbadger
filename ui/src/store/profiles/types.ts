import { Profile } from '../common/types';

export interface ProfileState {
  data: Profile[];
}
