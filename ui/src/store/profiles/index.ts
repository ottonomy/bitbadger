import { Module } from 'vuex';

import { RootState } from '../types';
import { Profile } from '../common/types';

import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { ProfileState } from './types';

const sampleProfile: Profile = {
  id: 'http://localhost:8000/profile',
  name: 'Test Issuer',
  description: 'This is a hardcoded test issuer profile',
  url: 'http://localhost:8000',
  email: 'test@example.com',
};
const state: ProfileState = {
  data: [sampleProfile],
};

const namespaced: boolean = true;

export const profiles: Module<ProfileState, RootState> = {
  namespaced,
  state,
  getters,
  actions,
  mutations,
};
