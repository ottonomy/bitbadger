import { GetterTree } from 'vuex';
import { RootState } from '../types';
import { Profile } from '../common/types';
import { ProfileState } from './types';


export const getters: GetterTree<ProfileState, RootState> = {
    currentProfile(state: ProfileState): Profile {
        return state.data[0];
    },
};
