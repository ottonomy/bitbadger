import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { RootState } from './types';
import { profiles } from './profiles/index';
import { badgeclasses } from './badgeclasses/index';


Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  modules: {
    profiles,
    badgeclasses,
  },
};

export default new Vuex.Store(store);
