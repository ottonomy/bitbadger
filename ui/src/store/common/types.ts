interface CryptographicKey {
  id: string;
  owner: string;
  publicKeyPem: string;
}

export interface Profile {
  id: string;
  name: string;
  description: string;
  email: string;
  url: string;
  publicKey?: CryptographicKey;
}
