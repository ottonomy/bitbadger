import { Profile } from './common/types';
import { ProfileState } from './profiles/types';
import { BadgeClassState } from './badgeclasses/types';

export interface Assertion {
  id: string;
  badge: string;
  issuedOn: string;
  image: string;
  evidence?: Evidence[];
  recipient: IdentityObject;
  narrative?: string;
  expires?: string;
  revoked?: boolean;
  revocationReason?: string;
  verification: VerificationObject;
}

interface Evidence {
  id?: string;
  narrative?: string;
  name?: string;
  description?: string;
  genre?: string;
  audience?: string;
}

interface IdentityObject {
  type: string;
  hashed: boolean;
  salt?: string;
  identity: string;
}

export interface RootState {
  profiles: ProfileState;
  badgeclasses: BadgeClassState;
  assertions: Assertion[];
}

interface VerificationObject {
  type: string;
  verificationProperty?: string;
  startsWith?: string;
  allowedOrigins?: string[];
}
