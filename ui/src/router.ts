import Vue from 'vue';
import Router from 'vue-router';
import Badges from './views/Badges.vue';
import BadgeDetail from './views/BadgeDetail.vue';
import Home from './views/Home.vue';
import Profile from './views/Profile.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
    },
    {
      path: '/badges',
      name: 'badges',
      component: Badges,
    },
    {
      path: '/badges/{id}',
      name: 'badges',
      component: Badges,
    },
  ],
});
