const ApiBuilder = require('claudia-api-builder'),
    AWS = require('aws-sdk'),
    jws = require('jws');
var api = new ApiBuilder(),
    dynamoDb = new AWS.DynamoDB.DocumentClient();
    
function request_error(msg) {
    return new ApiBuilder.ApiResponse(
        {error: msg},
        {'Content-Type': 'application/json'},
        400
    );
}

api.post('/badgeclass', function(request) { // SAVE your BadgeClass
    /* 
    Request.body is expected to be a JSON-encoded JWS signed using RSA
    */
    // Validate data is JWS:
    if (!request.body.header || !request.body.payload || !request.body.signature)
        return request_error('Please submit a JSON-encoded JWS with signed profile data.');

    // Validate header matches our initial expectation
    var header = request.body.header;
    if (!header.alg || header.alg != 'RS256')
        return request_error('Only RS256 alg supported at this time.');

    // Validate payload data:
    var payload = JSON.parse(Buffer.from(request.body.payload, 'base64').toString('utf-8'));
    if (!payload.issuer || !payload.name || !payload.description || !payload.image || !payload.criteria || !payload.criteria.narrative)
        return request_error('Missing required data: issuer, name, description, image, and criteria.narrative are required.');

    // Validate id format
    const id_pattern = RegExp(/^urn:uuid:[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}$/);
    if (!id_pattern.test(request.pathParams.id))
        return request_error('ID format not accepted. Must match urn:uuid:<uuid4> in 8-4-4-4-12 format.')

    // Validate issuer id format
    const issuer_id_pattern = RegExp(/^did:bitoftrustdemo:[a-zA-Z0-9]{1,20}$/);
    if (!issuer_id_pattern.test(payload.issuer))
        return request_error('Issuer ID format not accepted. Must match did:bitoftrustdemo:username with username 1-20 alphanumeric characters.')

    // Get issuer and issuer key from database
    return dynamoDb.get({
        TableName: 'bitbadger_profiles',
        Key: {
            id: payload.issuer
        }
    }).promise().then(function(response) {
        issuer = response.Item
    
        // Validate signature
        var publicKeyPem = issuer.publicKey.publicKeyPem;
        var compactJws = ['eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9', request.body.payload, request.body.signature].join('.')
        if (!jws.verify(compactJws, 'RS256', publicKeyPem))
            return request_error('Signature failed to verify');
        
        var params = {  
            TableName: 'bitbadger_badgeclasses',  
            Item: {
                id: payload.id,
                name: payload.name,
                description: payload.description,
                image: payload.image,
                criteria: payload.criteria,
                tags: payload.tags,
                issuer: payload.issuer,
                alignment: payload.alignment
            } 
        }
        return dynamoDb.put(params).promise(); // returns dynamo result
    });

    
    
     
}, { success: 201 }); // returns HTTP status 201 - Created if successful

api.get('/badgeclasses', function(request) { // GET all badgeclsses
  return dynamoDb.scan({ TableName: 'bitbadger_badgeclasses' }).promise()
      .then(response => response.Items)
});

api.get('/profiles/{id}', function(request) {
    return dynamoDb.get({
        TableName: 'bitbadger_profiles',
        Key: {
            id: request.pathParams.id
        }
    }).promise().then(response => response.Item);
}, { success: 200 })

api.put('/profiles/{id}', function(request) {
    /* 
    Request.body is expected to be a JSON-encoded JWS signed using RSA
    */
    // Validate data is JWS:
    if (!request.body.header || !request.body.payload || !request.body.signature)
        return request_error('Please submit a JSON-encoded JWS with signed profile data.');

    // Validate header matches our initial expectation
    var header = request.body.header;
    if (!header.alg || header.alg != 'RS256')
        return request_error('Only RS256 alg supported at this time.');

    // Validate payload data:
    var payload = JSON.parse(Buffer.from(request.body.payload, 'base64').toString('utf-8'));
    if (!payload.name || !payload.email || !payload.url || !payload.publicKey ||!payload.publicKey.publicKeyPem)
        return request_error('Missing required data: name, email, url, publicKey required.');

    // Validate id format
    const id_pattern = RegExp(/^did:bitoftrustdemo:[a-zA-Z0-9]{1,20}$/);
    if (!id_pattern.test(request.pathParams.id))
        return request_error('ID format not accepted. Must match did:bitoftrustdemo:username with username 1-20 alphanumeric characters.')

    // Validate signature
    var publicKeyPem = payload.publicKey.publicKeyPem;
    var compactJws = ['eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9', request.body.payload, request.body.signature].join('.')
    if (!jws.verify(compactJws, 'RS256', publicKeyPem))
        return request_error('Signature failed to verify');

    return dynamoDb.put({
        TableName: 'bitbadger_profiles',
        Item: {
            id: request.pathParams.id,
            name: payload.name,
            email: payload.email,
            url: payload.url,
            publicKey: {
                publicKeyPem: publicKeyPem
            } 
        }
    }).promise();
}, { success: 201 });

module.exports = api;